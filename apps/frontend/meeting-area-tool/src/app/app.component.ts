import { Component } from '@angular/core';

@Component({
  selector: 'slush-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend-meeting-area-tool-onkova';
}
