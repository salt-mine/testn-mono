module.exports = {
  name: 'backend-accounts',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/apps/backend/accounts'
};
