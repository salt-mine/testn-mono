module.exports = {
  name: 'backend-applications',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/apps/backend/applications'
};
